import React from 'react';
import light_bulb_off from './light-bulb-off.png';
import light_bulb_on from './light-bulb-on.png';
import Button from '@material-ui/core/Button';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { state: "off", ws: null };
  }
  componentDidMount() {
    var exampleSocket = new WebSocket("ws://localhost:4000/socket");
    const that = this;
    exampleSocket.onopen = function() {
      that.setState({ws : exampleSocket});

      exampleSocket.onmessage = function(event) {
        console.log(event);
        that.setState({state: event.data});
      }
    }
  }
  render() {
    const that = this;

    return (
      <div className="App">
        {
          this.state.state === "off" ?
          <img src={light_bulb_off} style={{height:400}} alt="light" />
          :
          <img src={light_bulb_on} style={{height:400}} alt="light" />
        }
        <div>
          <Button variant="contained" color="primary"
            onClick={() => that.state.ws.send("switch")}
            >
            Switch
          </Button>
          <Button variant="contained" color="secondary"
            onClick={() => that.state.ws.send("alarm")}
          >
            Alarm
          </Button>
        </div>
      </div>
    );
  }
}

export default App;
