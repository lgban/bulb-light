defmodule WsBe do
  @moduledoc """
  Documentation for WsBe.
  """

  @doc """
  Hello world.

  ## Examples

      iex> WsBe.hello()
      :world

  """
  def hello do
    :world
  end
end
