defmodule WsBe.Application do
  use Application

  def start(_type, _args) do
    children = [
      {Riverside, [handler: WsBe.CommHandler]}
    ]

    opts = [strategy: :one_for_one, name: WsBe.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
