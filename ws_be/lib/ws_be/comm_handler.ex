defmodule WsBe.CommHandler do
  use Riverside, otp_app: :ws_be

  @impl Riverside
  def init(session, state) do
    IO.inspect state
    {:ok, session, %{
      current: "off",
      timer_ref: nil,
      timers: %{
        {"alarm-on", 1} => "alarm-off",
        {"alarm-off", 1} => "alarm-on"
      },
      transitions: %{
        {"off", "switch"} => "on",
        {"on", "switch"} => "off",
        {"off", "alarm"} => "alarm-on",
        {"alarm-on", "switch"} => "off",
        {"alarm-off", "switch"} => "off"
      }
    }}
  end

  @impl Riverside
  def handle_message(event, session, state) do
    if Map.has_key?(state.transitions, {state.current, event}) do
      if state.timer_ref do
        Process.cancel_timer(state.timer_ref)
      end
  
      to = Map.get(state.transitions, {state.current, event})
      deliver_me(to)
      ref =
          case Enum.find(state.timers, fn {{source, _time}, _to} -> to == source end) do
            {{_, time}, target} ->
              Process.send_after(self(), {:alarm, target}, time * 1000)
            _ -> nil
          end
      {:ok, session, Map.put(state, :current, to) |> Map.put(:timer_ref, ref)}
    else 
      {:ok, session, state}
    end
  end

  def handle_info({:alarm, entered_state}, session, state) do
    ref =
      case Enum.find(state.timers, fn {{source, _time}, _to} -> entered_state == source end) do
        {{_, time}, target} ->
          Process.send_after(self(), {:alarm, target}, time * 1000)
        _ -> nil
      end
    case entered_state do
     "alarm-on" -> deliver_me("on")
     "alarm-off" -> deliver_me("off")
    end
    {:ok, session, Map.put(state, :current, entered_state) |> Map.put(:timer_ref, ref)}
  end
end