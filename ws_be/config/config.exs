import Config

config :ws_be, WsBe.CommHandler,
  port: 4000,
  path: "/socket",
  show_debug_logs: false,
  max_connection_age: :infinity,
  codec: Riverside.Codec.RawText
